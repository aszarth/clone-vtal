"use client";
import SvgGithub from "./svgs/github.svg";
import SvgInternet from "./svgs/internet-explorer.svg";
import SvgInstagram from "./svgs/instagram.svg";
import SvgLinkedin from "./svgs/linkedin.svg";
import SvgSortDown from "./svgs/sort-down.svg";
import SvgChevronLeft from "./svgs/chevron-left.svg";
import SvgChevronRight from "./svgs/chevron-right.svg";
import SvgAlignRight from "./svgs/align-right.svg";

export default function Icon({
  name,
  width,
  height,
  color,
  style,
  onClick,
}: {
  name:
    | "internet"
    | "git"
    | "instagram"
    | "linkedin"
    | "sort-down"
    | "chevron-left"
    | "chevron-right"
    | "align-right";
  width: number;
  height: number;
  color: string;
  style?: object;
  onClick?: Function;
}) {
  return (
    <div
      style={{ width: width, height: height, ...style }}
      onClick={() => {
        if (onClick) onClick();
      }}
    >
      {name == "internet" && (
        <SvgInternet fill={color} width={width} height={height} />
      )}
      {name == "git" && (
        <SvgGithub fill={color} width={width} height={height} />
      )}
      {name == "linkedin" && (
        <SvgLinkedin fill={color} width={width} height={height} />
      )}
      {name == "instagram" && (
        <SvgInstagram fill={color} width={width} height={height} />
      )}
      {name == "sort-down" && (
        <SvgSortDown fill={color} width={width} height={height} />
      )}
      {name == "chevron-left" && (
        <SvgChevronLeft fill={color} width={width} height={height} />
      )}
      {name == "chevron-right" && (
        <SvgChevronRight fill={color} width={width} height={height} />
      )}
      {name == "align-right" && (
        <SvgAlignRight fill={color} width={width} height={height} />
      )}
    </div>
  );
}
