"use client";
import { useState } from "react";
import Icon from "./Icon";

interface ImgCarouselProps {
  src: string;
  title: string;
  desc: string;
}
export default function ImageOrCarousel({
  imgs,
  isMobile,
}: {
  imgs: ImgCarouselProps[];
  isMobile: boolean;
}) {
  const [imgSelected, setImageSelected] = useState(0);
  return (
    <div
      style={{
        width: "100vw",
        height: 550,
        backgroundImage: `url("${imgs[imgSelected].src}")`,
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      <TitleDescImageCarousel
        imgs={imgs}
        imgSelected={imgSelected}
        isMobile={isMobile}
      />
      <NextPreviousCarousel
        imgs={imgs}
        imgSelected={imgSelected}
        setImageSelected={setImageSelected}
        isMobile={isMobile}
      />
      <ImageSquareCarousel
        imgs={imgs}
        imgSelected={imgSelected}
        setImageSelected={setImageSelected}
        isMobile={isMobile}
      />
    </div>
  );
}

function TitleDescImageCarousel({
  imgs,
  imgSelected,
  isMobile,
}: {
  imgs: ImgCarouselProps[];
  imgSelected: number;
  isMobile: boolean;
}) {
  return (
    <div
      style={{
        width: !isMobile ? 500 : 300,
        height: 300,
        position: "absolute",
        display: "flex",
        flexDirection: "column",
        top: 180,
        right: "10%",
        textAlign: "end",
      }}
    >
      <span
        style={{
          fontFamily: "Roboto",
          fontSize: !isMobile ? 36 : 22,
          fontWeight: "bold",
          color: "#FFF",
        }}
      >
        {imgs[imgSelected].title}
      </span>
      <span
        style={{
          fontFamily: "Roboto",
          fontSize: !isMobile ? 30 : 18,
          lineHeight: 1.6,
          fontWeight: 300,
          color: "#FFF",
          marginTop: 10,
          textAlign: "right",
        }}
      >
        {imgs[imgSelected].desc}
      </span>
    </div>
  );
}

function NextPreviousCarousel({
  imgs,
  imgSelected,
  setImageSelected,
  isMobile,
}: {
  imgs: ImgCarouselProps[];
  imgSelected: number;
  setImageSelected: Function;
  isMobile: boolean;
}) {
  return (
    <div>
      <Icon
        name="chevron-left"
        width={32}
        height={32}
        color="#FFF"
        style={{
          position: "absolute",
          top: !isMobile ? 250 : 350,
          cursor: "pointer",
        }}
        onClick={() => {
          if (imgSelected <= 0) {
            setImageSelected(imgs.length - 1);
          } else {
            setImageSelected(imgSelected - 1);
          }
        }}
      />
      <Icon
        name="chevron-right"
        width={32}
        height={32}
        color="#FFF"
        style={{
          position: "absolute",
          top: !isMobile ? 250 : 350,
          right: 1,
          cursor: "pointer",
        }}
        onClick={() => {
          if (imgSelected >= imgs.length - 1) {
            setImageSelected(0);
          } else {
            setImageSelected(imgSelected + 1);
          }
        }}
      />
    </div>
  );
}

function ImageSquareCarousel({
  imgs,
  imgSelected,
  setImageSelected,
  isMobile,
}: {
  imgs: ImgCarouselProps[];
  imgSelected: number;
  setImageSelected: Function;
  isMobile: boolean;
}) {
  return (
    <div
      style={{
        position: "absolute",
        top: !isMobile ? 520 : 490,
        right: isMobile ? "42%" : "46%",
        display: "flex",
        flexDirection: "row",
      }}
    >
      {imgs.map((e, i) => {
        return (
          <div
            key={e.src}
            style={{
              width: 14,
              height: 14,
              borderRadius: 1,
              borderColor: "#FFF",
              borderStyle: "solid",
              borderWidth: 2,
              marginLeft: 10,
              cursor: "pointer",
              backgroundColor: imgSelected == i ? "#FFF" : undefined,
            }}
            onClick={() => setImageSelected(i)}
          />
        );
      })}
    </div>
  );
}
