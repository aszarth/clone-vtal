"use client";
import { useState } from "react";
import Icon from "./Icon";

export default function Footer({ isMobile }: { isMobile: boolean }) {
  return (
    <div>
      <HeadFooter />
      <NavFooter isMobile={isMobile} />
      <AllRightsFooter />
    </div>
  );
}

function HeadFooter() {
  const ICON_SIZE = 32;
  const [isMouseHoverLinkedin, setIsMouseHoverLinkedin] = useState(false);
  const [isMouseHoverInsta, setIsMouseHoverInsta] = useState(false);
  return (
    <div
      style={{
        backgroundColor: "#464643",
        height: 320,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "space-around",
        padding: "50px 0",
      }}
    >
      <img src="/logo_footer.png" width={250} height={100} />
      <div
        style={{ width: "70%", height: 1, backgroundColor: "#F1FF00" }}
      ></div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div
          onMouseEnter={() => setIsMouseHoverLinkedin(true)}
          onMouseLeave={() => setIsMouseHoverLinkedin(false)}
          style={{ marginRight: 10 }}
        >
          <Icon
            name="linkedin"
            width={ICON_SIZE}
            height={ICON_SIZE}
            color={isMouseHoverLinkedin ? "#F1FF00" : "#FFF"}
            style={{ cursor: "pointer" }}
          />
        </div>
        <div
          onMouseEnter={() => setIsMouseHoverInsta(true)}
          onMouseLeave={() => setIsMouseHoverInsta(false)}
          style={{ marginLeft: 10 }}
        >
          <Icon
            name="instagram"
            width={ICON_SIZE}
            height={ICON_SIZE}
            color={isMouseHoverInsta ? "#F1FF00" : "#FFF"}
            style={{ cursor: "pointer" }}
          />
        </div>
      </div>
    </div>
  );
}

function NavFooter({ isMobile }: { isMobile: boolean }) {
  return (
    <div
      style={{
        backgroundColor: "#B2B2B2",
        color: "#464643",
        width: "100%",
        height: !isMobile ? 80 : 200,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <div
        style={{
          width: !isMobile ? "35%" : undefined,
          display: "flex",
          flexDirection: !isMobile ? "row" : "column",
          justifyContent: !isMobile ? "space-between" : "center",
          alignItems: "center",
          paddingBottom: isMobile ? 25 : undefined,
        }}
      >
        <NavFooterItem isMobile={isMobile}>Ethics and Integrity</NavFooterItem>
        {!isMobile && <NavFooterSeparator />}
        <NavFooterItem isMobile={isMobile}>Privacy policy</NavFooterItem>
        {!isMobile && <NavFooterSeparator />}
        <NavFooterItem isMobile={isMobile}>V.tal Policies</NavFooterItem>
        {!isMobile && <NavFooterSeparator />}
        <NavFooterItem isMobile={isMobile}>Investors</NavFooterItem>
      </div>
    </div>
  );
}

function NavFooterSeparator() {
  return (
    <span
      style={{
        color: "#000",
        fontFamily: `"Inter", Sans-serif`,
        fontSize: 13,
        fontWeight: 400,
      }}
    >
      {" "}
      |{" "}
    </span>
  );
}

function NavFooterItem({
  children,
  isMobile,
}: {
  children: string;
  isMobile: boolean;
}) {
  const [isMouseHover, setIsMouseHover] = useState(false);
  return (
    <div style={{ marginTop: isMobile ? 25 : undefined }}>
      <span
        style={{
          cursor: "pointer",
          fontFamily: `"Inter", Sans-serif`,
          fontSize: !isMobile ? 13 : 18,
          fontWeight: 400,
          color: isMouseHover ? "#F1FF00" : "#464643",
        }}
        onMouseEnter={() => setIsMouseHover(true)}
        onMouseLeave={() => setIsMouseHover(false)}
      >
        {children}
      </span>
    </div>
  );
}

function AllRightsFooter() {
  return (
    <div
      style={{
        height: 60,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <span
        style={{
          color: "#7A7A7A",
          fontSize: 14,
          fontFamily: `"Inter", Sans-serif`,
          textAlign: "center",
        }}
      >
        © 2021 V.tal. All rights reserved.
      </span>
    </div>
  );
}
