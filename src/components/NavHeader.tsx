"use client";
import { useEffect, useState } from "react";
import Icon from "./Icon";

export default function NavHeader() {
  const [isAtTop, setIsAtTop] = useState(true);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY === 0) {
        setIsAtTop(true);
      } else {
        setIsAtTop(false);
      }
    };
    // Add scroll event listener
    window.addEventListener("scroll", handleScroll);
    // Call handleScroll on mount to set the initial state
    handleScroll();
    // Cleanup the event listener on unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div
      style={{
        backgroundColor: "#404040",
        height: isAtTop ? 70 : 60,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingLeft: 50,
        paddingRight: 50,
        position: "fixed",
        width: "100vw",
        zIndex: 10,
        boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
      }}
    >
      <img
        src="/logo_header.png"
        width={isAtTop ? 140 : 90}
        height={isAtTop ? 40 : 30}
        style={{
          transition: "width 0.5s ease, height 0.5s ease",
        }}
      />
      <div
        className="navItems"
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <NavItem title="About Us" selected={true} />
        <NavItem title="Products" />
        <NavItem title="Contact Us" />
        <NavItem title="Developers" />
        <NavItem title="Press" />
        <IdiomSelected isMobile={false} />
        <SocialMedias />
      </div>
    </div>
  );
}

export function NavHeaderMobile() {
  const [isAtTop, setIsAtTop] = useState(true);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY === 0) {
        setIsAtTop(true);
      } else {
        setIsAtTop(false);
      }
    };
    // Add scroll event listener
    window.addEventListener("scroll", handleScroll);
    // Call handleScroll on mount to set the initial state
    handleScroll();
    // Cleanup the event listener on unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <div
      style={{
        height: isAtTop ? 90 : 70,
        width: "100vw",
        backgroundColor: "#404040",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        padding: "0px 20px",
        position: "fixed",
        zIndex: 10,
        boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
      }}
    >
      <img
        src="/logo_header.png"
        width={isAtTop ? 150 : 120}
        height={isAtTop ? 50 : 30}
        style={{
          transition: "width 0.5s ease, height 0.5s ease",
        }}
      />
      <div style={{ display: "flex", alignItems: "center" }}>
        <IdiomSelected isMobile={true} />
        <div
          style={{
            width: 50,
            height: 50,
            backgroundColor: "#464643",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Icon name="align-right" width={32} height={32} color="#F1FF00" />
        </div>
      </div>
    </div>
  );
}

function NavItem({ title, selected }: { title: string; selected?: boolean }) {
  const [isMouseOn, setIsMouseOn] = useState(false);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 25,
        height: 30,
      }}
      onMouseEnter={() => setIsMouseOn(true)}
      onMouseLeave={() => setIsMouseOn(false)}
    >
      <span
        style={{
          color: selected ? "#F1FF00" : "#FFF",
          fontSize: 15,
          fontWeight: 300,
          cursor: "pointer",
          marginBottom: isMouseOn ? 5 : 0,
          transition: "margin 500ms",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {title}
      </span>
      <div
        style={{
          width: "100%",
          transition: "height 500ms",
          height: isMouseOn ? 4 : 0,
          backgroundColor: "#F1FF00",
        }}
      />
    </div>
  );
}

function IdiomSelected({ isMobile }: { isMobile: boolean }) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        margin: !isMobile ? "0px 50px" : "0px 25px",
      }}
    >
      <span
        style={{
          fontSize: !isMobile ? 14 : 18,
          color: "#7A7A7A",
          height: "100%",
          fontWeight: "bold",
        }}
      >
        EN
      </span>
      <Icon
        name="sort-down"
        width={!isMobile ? 14 : 18}
        height={!isMobile ? 14 : 18}
        color="#7A7A7A"
        style={{ height: "100%", paddingBottom: 5, marginLeft: 2 }}
      />
    </div>
  );
}

function SocialMedias() {
  const [isMouseHoverLinkedin, setIsMouseHoverLinkedin] = useState(false);
  const [isMouseHoverInsta, setIsMouseHoverInsta] = useState(false);
  const ICON_SIZE = 22;
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        fontFamily: `"Inter", Sans-serif`,
        fontWeight: 300,
      }}
    >
      <div
        onMouseEnter={() => setIsMouseHoverLinkedin(true)}
        onMouseLeave={() => setIsMouseHoverLinkedin(false)}
      >
        <Icon
          name="linkedin"
          width={ICON_SIZE}
          height={ICON_SIZE}
          color={isMouseHoverLinkedin ? "#F1FF00" : "#FFF"}
          style={{ cursor: "pointer" }}
        />
      </div>
      <div
        onMouseEnter={() => setIsMouseHoverInsta(true)}
        onMouseLeave={() => setIsMouseHoverInsta(false)}
        style={{ marginLeft: 15 }}
      >
        <Icon
          name="instagram"
          width={ICON_SIZE}
          height={ICON_SIZE}
          color={isMouseHoverInsta ? "#F1FF00" : "#FFF"}
          style={{ cursor: "pointer" }}
        />
      </div>
    </div>
  );
}
