"use client";

import { useState } from "react";

export default function Button({
  type,
  width,
  height,
  borderRadius,
  children,
  onClick,
  fontSize,
  style,
}: {
  type: "primary" | "secondary";
  width: number | string;
  height: number | string;
  borderRadius?: number;
  children: string;
  onClick?: Function;
  fontSize?: number;
  style?: object;
}) {
  const primaryColor = "#404040";
  const secondaryColor = "#F1FF00";
  const [isMouseHover, setIsMouseHover] = useState(false);
  let bgColor;
  let txtColor;
  if (type == "primary") {
    if (isMouseHover) {
      bgColor = secondaryColor;
      txtColor = primaryColor;
    } else {
      bgColor = primaryColor;
      txtColor = secondaryColor;
    }
  } else if (type == "secondary") {
    if (isMouseHover) {
      bgColor = primaryColor;
      txtColor = secondaryColor;
    } else {
      bgColor = secondaryColor;
      txtColor = primaryColor;
    }
  }
  return (
    <div
      style={{
        width: width,
        height: height,
        borderRadius: borderRadius,
        backgroundColor: bgColor,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        cursor: "pointer",
        ...style,
      }}
      onClick={() => {
        if (onClick) onClick();
      }}
      onMouseEnter={() => setIsMouseHover(true)}
      onMouseLeave={() => setIsMouseHover(false)}
    >
      <span
        style={{
          color: txtColor,
          fontSize: fontSize ? fontSize : 16,
        }}
      >
        {children}
      </span>
    </div>
  );
}
