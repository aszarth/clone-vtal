"use client";
import Button from "@/components/Button";
import CheckIsMobile from "@/libs/checkIsMobile";
import { useEffect, useState } from "react";

export default function Home() {
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    setIsMobile(CheckIsMobile());
  }, []);
  return (
    <div>
      <AboutUs isMobile={isMobile} />
      <Cases isMobile={isMobile} />
      <AboutOurServices isMobile={isMobile} />
      <BusinessUnits isMobile={isMobile} />
      <PreFooter isMobile={isMobile} />
    </div>
  );
}

function AboutUs({ isMobile }: { isMobile: boolean }) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: 400,
      }}
    >
      <span
        style={{
          fontSize: isMobile ? 28 : 42,
          lineHeight: "1.4em",
          color: "#464643",
          textAlign: "center",
          justifyContent: "center",
          marginTop: !isMobile ? 50 : 0,
          paddingLeft: "10%",
          paddingRight: "10%",
        }}
      >
        Global provider of <b>end-to-end digital infrastructure solutions</b>{" "}
        and owner of <b>the largest neutral fiber optic network in Brazil</b>.
      </span>
      <span
        style={{
          fontSize: 16,
          color: "#464643",
          fontWeight: 400,
          lineHeight: "1.4em",
          textAlign: "center",
          marginTop: 25,
          width: !isMobile ? 500 : undefined,
        }}
      >
        We share our neutral infrastructure with operators, providers,
        telecommunications startups and OTTs (content distributors)
      </span>
    </div>
  );
}

function Cases({ isMobile }: { isMobile: boolean }) {
  return (
    <div
      style={{
        backgroundColor: "#6D6D6D",
        marginTop: !isMobile ? 30 : 0,
        height: !isMobile ? 300 : 1000,
        display: "flex",
        flexDirection: !isMobile ? "row" : "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CaseCard
        src="/icon-brasil.png"
        title="Optical Fiber"
        desc="digital infrastructure"
        isMobile={isMobile}
      />
      <CaseCard
        src="/icon-casa.png"
        title="22 million"
        desc="homes passed with FTTH"
        isMobile={isMobile}
      />
      <CaseCard
        src="/cabo.png"
        title="26 thousand km"
        desc="of submarine cables"
        isMobile={isMobile}
      />
      <CaseCard
        src="/icon-datacenter.png"
        title="Data centers"
        desc="In Brazil and Colombia"
        isMobile={isMobile}
      />
    </div>
  );
}

function CaseCard({
  src,
  title,
  desc,
  isMobile,
}: {
  src: string;
  title: string;
  desc: string;
  isMobile: boolean;
}) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: 250,
        height: 200,
        marginTop: isMobile ? 30 : 0,
      }}
    >
      <img src={src} width={100} height={100} />
      <span
        style={{
          textAlign: "center",
          color: "#FFF",
          fontFamily: `"Inter", Sans-serif`,
          fontSize: "22px",
          fontWeight: "bold",
          lineHeight: "19px",
          marginTop: 10,
        }}
      >
        {title}
      </span>
      <span
        style={{
          textAlign: "center",
          fontFamily: `"Inter", Sans-serif`,
          fontSize: "20px",
          fontWeight: 300,
          lineHeight: "19px",
          marginTop: 10,
          color: "#F0F8FF",
        }}
      >
        {desc}
      </span>
      <div
        style={{
          backgroundColor: "#F1FF00",
          width: 60,
          height: 3,
          marginTop: 15,
        }}
      />
    </div>
  );
}

function AboutOurServices({ isMobile }: { isMobile: boolean }) {
  return (
    <div style={{ marginLeft: !isMobile ? "20%" : 0, height: 350 }}>
      <div
        style={{
          width: !isMobile ? 500 : undefined,
          marginTop: 50,
          marginLeft: isMobile ? 15 : undefined,
          marginRight: isMobile ? 15 : undefined,
        }}
      >
        <span
          style={{
            fontSize: !isMobile ? 42 : 28,
            lineHeight: "53px",
            color: "#464643",
          }}
        >
          Everything that is <b>vital passes through</b> here
        </span>
      </div>
      <div
        style={{
          fontSize: 16,
          color: "#464643",
          fontWeight: 400,
          lineHeight: "27px",
          marginTop: 25,
          width: !isMobile ? 500 : undefined,
          display: "flex",
          flexDirection: "column",
          marginLeft: isMobile ? 15 : undefined,
          marginRight: isMobile ? 15 : undefined,
        }}
      >
        <span>
          We offer an integrated portfolio of digital solutions across the
          Americas.
        </span>
        <span style={{ marginTop: 10 }}>
          We enable 5G and emerging technologies such as artificial
          intelligence, the Internet of Things, and augmented and virtual
          reality.
        </span>
      </div>
    </div>
  );
}

function BusinessUnits({ isMobile }: { isMobile: boolean }) {
  return (
    <div
      style={{
        backgroundColor: "#C6C6C6",
        fontSize: 28,
        height: !isMobile ? 800 : 1600,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <span
          style={{
            fontSize: 28,
            color: "#464643",
            textAlign: "center",
            fontWeight: "bold",
            marginTop: 100,
            marginBottom: !isMobile ? 100 : 50,
          }}
        >
          Business Units
        </span>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: !isMobile ? "row" : "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <BusinessUnitCard
          src="/icon-produto-ftth.png"
          title="FTTH"
          content="Speeds up to 2Gbps with customized solutions for the customer’s home"
          isMobile={isMobile}
        />
        <BusinessUnitCard
          src="/connection.png"
          title="Connectivity"
          content="Wholesale national and international connectivity solutions"
          isMobile={isMobile}
        />
        <BusinessUnitCard
          src="/datacenter.png"
          title="Data center"
          content="High quality infrastructure and neutral ecosystems"
          isMobile={isMobile}
        />
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          marginTop: 80,
        }}
      >
        <Button type="primary" width={300} height={50} borderRadius={25}>
          Discover our complete portfolio
        </Button>
      </div>
    </div>
  );
}

function BusinessUnitCard({
  src,
  title,
  content,
  isMobile,
}: {
  src: string;
  title: string;
  content: string;
  isMobile: boolean;
}) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#FFF",
        width: 350,
        height: 350,
        borderRadius: 25,
        margin: "0px 50px",
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 2,
        borderStyle: "solid",
        borderColor: "#F1FF00",
        boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
        marginTop: isMobile ? 25 : 0,
      }}
    >
      <img src={src} width={150} height={150} />
      <span
        style={{
          fontSize: 22,
          color: "#464643",
          fontWeight: "bold",
          marginTop: 10,
        }}
      >
        {title}
      </span>
      <span
        style={{
          fontSize: 17,
          textAlign: "center",
          color: "#464643",
          marginTop: 10,
        }}
      >
        {content}
      </span>
    </div>
  );
}

function PreFooter({ isMobile }: { isMobile: boolean }) {
  const imgBg = "/bgprefooter.png";
  return (
    <div
      style={{
        width: "100vw",
        height: 600,
        backgroundImage: `url("${imgBg}")`,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <div
        style={{
          width: !isMobile ? 800 : "90vw",
          height: 400,
          borderRadius: 20,
          backgroundColor: "#FFF",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
          borderWidth: 2,
          borderStyle: "solid",
          borderColor: "#F1FF00",
        }}
      >
        <span
          style={{
            fontSize: !isMobile ? 42 : 28,
            color: "#464643",
            lineHeight: !isMobile ? "53px" : undefined,
            fontWeight: 400,
            textAlign: "center",
            marginTop: 25,
            marginLeft: 25,
            marginRight: 25,
          }}
        >
          We connect the world to a more sustainable future,{" "}
          <b>enabling digital transformation</b>.
        </span>
        <span
          style={{
            color: "#7A7A7A",
            fontSize: 16,
            textAlign: "center",
            fontWeight: 400,
            marginTop: 25,
            marginLeft: 25,
            marginRight: 25,
          }}
        >
          Future-proof digital infrastructure to build the future, in
          partnership with our clients, today
        </span>
        <Button
          type="secondary"
          width={150}
          height={50}
          borderRadius={25}
          style={{ marginTop: !isMobile ? 50 : 25 }}
        >
          Contact Us
        </Button>
      </div>
    </div>
  );
}
